<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    //GET AND INSERT PRODUK
    public function index()
    {
        $data['title'] = 'Daftar Produk';
        $data['user'] = $this->db->get_where('user', [
            'email' => $this->session->userdata('email'),
        ])->row_array();

        $data['content'] = 'produk/index';

        $this->load->view('layout', $data);

    }

    // GET AND INSERT KATEGORI
    public function kategori()
    {
        $data['title'] = 'Kategori Produk';
        $data['user'] = $this->db->get_where('user', [
            'email' => $this->session->userdata('email'),
        ])->row_array();

        $data['content'] = 'produk/kategori';
        $data['kategori'] = $this->db->get('kategori_produk')->result_array();

        $this->form_validation->set_rules('kategori', 'Kategori', "required", [
            'required' => 'Kategori wajib diisi',
        ]);

        //Insert data ke tabel kategori_produk
        if ($this->form_validation->run() == false) {
            $this->load->view('layout', $data);
        } else {
            $this->db->insert('kategori_produk', [
                'nama' => $this->input->post('kategori'),
            ]);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Kategori baru ditambah </div>');
            redirect('produk/kategori');

        }
    }

    //UBAH MENU
    public function getUbahKategori()
    {
        $id = $_POST['id'];
        $this->load->model('Produk_model', 'produk');
        echo json_encode($this->produk->getKategori($id));
    }

    public function ubahKategori()
    {
        $id = $_POST['id'];
        $this->load->model('Produk_model', 'produk');
        $this->produk->editKategori($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Kategori diubah! </div>');
        redirect('produk/kategori');
    }

    //HAPUS KATEGORI
    public function hapusKategori($id)
    {
        $this->load->model('Produk_model', "produk");
        $this->produk->deleteKategori($id);
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Kategori terhapus </div>');
        redirect('produk/kategori');
    }

}