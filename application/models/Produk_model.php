<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk_model extends CI_Model
{
    //GET KATEGORI BY ID
    public function getKategori($id)
    {
        // echo $_POST[$id];
        // $query = "SELECT * FROM `user_menu` WHERE `id` ='.$id";

        return $this->db->get_where('kategori_produk', ['id' => $id])->row_array();
    }

    //EDIT KATEGORI
    public function editKategori()
    {
        $data = [
            "nama" => $this->input->post('kategori', true),
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('kategori_produk', $data);
    }
    //DELETE MENU
    public function deleteKategori($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('kategori_produk');
    }
}