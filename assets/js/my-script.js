$(function () {
	//Add Menu
	$(".tombolTambahMenu").on("click", function () {
		$("#newMenuModalLabel").html("Tambah Menu ");
		$(".modal-footer button[type=submit]").html("Tambah");
		$("#menu").val("");
		$("#urutan").val("");
		$("#id").val("");
	});

	//Edit Menu from Add Menu
	$(".tampilModalUbahMenu").on("click", function () {
		$("#newMenuModalLabel").html("Edit Menu ");
		$(".modal-footer button[type=submit]").html("Edit");
		$(".modal-body form").attr(
			"action",
			"http://localhost/pos-rajawali/menu/ubahMenu"
		);

		const id = $(this).data("id");

		$.ajax({
			url: "http://localhost/pos-rajawali/menu/getUbahMenu",
			data: {
				id: id,
			},
			method: "post",
			dataType: "json",
			success: function (data) {
				$("#menu").val(data.menu);
				$("#urutan").val(data.urutan);
				$("#id").val(data.id);
			},
		});
	});

	// Edit and get Data SubMenu
	$(".tampilModalUbahSubMenu").on("click", function () {
		$("#newSubMenuModalLabel").html("Edit Sub Menu");
		$(".modal-footer button[type=submit]").html("Edit");
		$(".modal-body form").attr(
			"action",
			"http://localhost/pos-rajawali/menu/ubahSubMenu"
		);
		// $.ajax({
		// 	url: "http://localhost/pos-rajawali/menu/ubahSubMenu",
		// 	data: {
		// 		id: id,
		// 	},
		// 	method:post
		// });

		const id = $(this).data("id");

		// console.log(data);

		$.ajax({
			url: "http://localhost/pos-rajawali/menu/getUbahSubMenu",
			data: {
				id: id,
			},
			method: "post",
			dataType: "json",
			success: function (data) {
				// console.log(data);
				$("#title").val(data.judul);
				$("#url").val(data.url);
				$("#menu_id").val(data.menu_id);
				$("#is_active").val(data.is_active);
				$("#is_active").prop("checked", data.is_active == 1 ? true : false);
				$("#is_active").val(
					($valTemp = data.is_active)

					// 	"checked = true"
					// 	data.is_active = 1
				);
				$("#icon").val(data.icon);
				$("#id").val(data.id);
			},
		});
		// console.log(test);
	});

	//Add SubMenu
	$(".tombolTambahSubMenu").on("click", function () {
		$("#newSubMenuModalLabel").html("Tambah Sub Menu");
		$(".modal-footer button[type=submit]").html("Tambah");
		$("#title").val("");
		$("#url").val("");
		$("#menu_id").val("");
		$("#icon").val("");
		$("#is_active").val("1");
		$("#id").val("");
	});

	//Add Role
	$(".tombolTambahRole").on("click", function () {
		$("#newRoleModalLabel").html("Tambah User ");
		$(".modal-footer button[type=submit]").html("Tambah");
		$("#role").val("");
		$("#id").val("");
	});

	//Edit Role from Add Role
	$(".tampilModalUbahRole").on("click", function () {
		$("#newRoleModalLabel").html("Edit User ");
		$(".modal-footer button[type=submit]").html("Edit");
		$(".modal-body form").attr(
			"action",
			"http://localhost/pos-rajawali/admin/ubahRole"
		);

		const id = $(this).data("id");

		$.ajax({
			url: "http://localhost/pos-rajawali/admin/getUbahRole",
			data: {
				id: id,
			},
			method: "post",
			dataType: "json",
			success: function (data) {
				$("#role").val(data.role);
				$("#id").val(data.id);
			},
		});
	});

	//Add Kategori
	$(".tombolTambahKategori").on("click", function () {
		$("#newKategoriModalLabel").html("Tambah Kategori");
		$(".modal-footer button[type=submit]").html("Tambah");
		$("#kategori").val("");
		$("#id").val("");
	});

	//Edit Kategori from Add Kategori
	$(".tampilModalUbahKategori").on("click", function () {
		$("#newKategoriModalLabel").html("Edit Kategori ");
		$(".modal-footer button[type=submit]").html("Edit");
		$(".modal-body form").attr(
			"action",
			"http://localhost/pos-rajawali/produk/ubahKategori"
		);

		const id = $(this).data("id");

		$.ajax({
			url: "http://localhost/pos-rajawali/produk/getUbahKategori",
			data: {
				id: id,
			},
			method: "post",
			dataType: "json",
			success: function (data) {
				$("#kategori").val(data.kategori);
				$("#id").val(data.id);
			},
		});
	});
});
